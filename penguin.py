p1 = '   _~_     '
p2 = '  (o o)    '
p3 = ' /  V  \   '
p4 = '/(  _  )\  '
p5 = '  ^^ ^^    '

times = int(input())
print(p1 * times, p2 * times, p3 * times, p4 * times, p5 * times, sep='\n')
