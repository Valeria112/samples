import doctest


def find_max_subarray_len(arr):
    """
    >>> find_max_subarray_len([1, 2, 3])
    0

    >>> find_max_subarray_len([1, 2, 3, 0])
    1

    >>> find_max_subarray_len([1, 2, 3, -3])
    2

    >>> find_max_subarray_len([1, 1, 1, 3, 4, -7, -2, 2])
    6

    >>> find_max_subarray_len([1, 2, 3, -3, -2, -1, 0])
    7

    >>> find_max_subarray_len([15, -2, 2, -8, 1, 7, 10, 23])
    5

    """
    partial_sum = max_len = 0
    counts = {0: -1}

    for x in range(len(arr)):
        partial_sum += arr[x]

        idx = counts.get(partial_sum)

        if idx is not None:
            tmp_len = x - idx

            if tmp_len > max_len:
                max_len = tmp_len
        else:
            counts[partial_sum] = x

    return max_len


if __name__ == '__main__':
    doctest.testmod()
