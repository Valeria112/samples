import doctest


def is_palindrome(string):
    """
    >>> is_palindrome('abc')
    False

    >>> is_palindrome('a bc ')
    False

    >>> is_palindrome('aabbaa')
    True

    >>> is_palindrome('aa b baa')
    True

    >>> is_palindrome('aa b baa')
    True

    >>> is_palindrome('aabaa')
    True

    >>> is_palindrome('aab    aa')
    True

    """
    start = 0
    end = len(string) - 1

    while start < end:

        if string[start] == ' ':
            start += 1
            continue

        if string[end] == ' ':
            end -= 1
            continue

        if string[start] != string[end]:
            return False

        end -= 1
        start += 1

    return True


if __name__ == '__main__':
    doctest.testmod()
