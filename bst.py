class TreeNode:
    """
    Binary Search Tree Node
    """

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

    def traverse_preorder(self):
        print(self.data)

        if self.left is not None:
            self.left.traverse_preorder()

        if self.right is not None:
            self.right.traverse_preorder()

    def traverse_inorder(self):
        if self.left is not None:
            self.left.traverse_preorder()

        print(self.data)

        if self.right is not None:
            self.right.traverse_preorder()

    def traverse_postorder(self):
        if self.left is not None:
            self.left.traverse_preorder()

        if self.right is not None:
            self.right.traverse_preorder()

        print(self.data)

    def traverse_depth_first(self):
        queue = [self]

        while queue:
            tmp = queue.pop(0)
            print(tmp.data)

            if tmp.left is not None:
                queue.append(tmp.left)

            if tmp.right is not None:
                queue.append(tmp.right)

    def add_node(self, data):
        if data < self.data:
            if self.left is not None:
                self.left.add_node(data)
            else:
                self.left = TreeNode(data)
        else:
            if self.right is not None:
                self.right.add_node(data)
            else:
                self.right = TreeNode(data)

    def search_node(self, target):
        if self.data == target:
            return self

        if target < self.data:
            return None if self.left is None else self.left.search_node(target)
        else:
            return None if self.right is None else self.right.search_node(target)

    def delete_node(self, parent, is_left):
        node = parent.left if is_left else parent.right
        if node is None:
            return

        if node.left is None and node.right is None:
            if is_left:
                parent.left = None
            else:
                parent.right = None
            return

        elif node.left is None:
            node.data = node.right.data
            return node.right.delete_node(node, False)

        elif node.right is None:
            node.data = node.left.data
            return node.left.delete_node(node, True)

        else:
            tmp = node.left
            if tmp.right is None:
                parent.left = tmp
            else:
                while tmp.right.right:
                    tmp = tmp.right

                node.data = tmp.right.data

                if tmp.right.left is None:
                    tmp.right = None
                else:
                    tmp.right.data = tmp.right.left.data
                    tmp.right.left = tmp.right.left.left


if __name__ == '__main__':
    head = TreeNode(10)
    head.add_node(1)
    head.add_node(11)

    # head.traverse_preorder()
    # head.traverse_inorder()
    # head.traverse_postorder()
    head.traverse_depth_first()
