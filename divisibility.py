"""
check if "a" shares whole by "b"
using only arithmetic operations

print 'YES' if "a" shares whole by "b"
and 'NO' otherwise
"""

a = int(input())
b = int(input())

rest = a % b
coeff = (2 * rest // (rest + 1))

no = 'NO' * coeff
yes = 'YES' * (1 - coeff)

print(no + yes)
