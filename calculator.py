import doctest


def op_precedence(op):
    """
    >>> op_precedence('+')
    0
    >>> op_precedence('-')
    0
    >>> op_precedence('*')
    1
    >>> op_precedence('/')
    1
    """
    return 1 if op in ('*', '/') else 0


def covert_to_rpn(expression):
    """
    conver expression in infix notstion to reverse polish notation

    >>> covert_to_rpn('2+ 3')
    [2, 3, '+']

    >>> covert_to_rpn('2 + 3 * 8')
    [2, 3, 8, '*', '+']

    >>> covert_to_rpn('(2 + 3) * 8')
    [2, 3, '+', 8, '*']

    >>> covert_to_rpn('(2 / 3 + 3) * 7 - 1')
    [2, 3, '/', 3, '+', 7, '*', 1, '-']

    """
    queue = []
    stask = []

    i = 0
    l = len(expression)

    while i < l:
        token = expression[i]
        i += 1

        if token.isdigit():
            while i < l and expression[i].isdigit():
                token = ''.join([token, expression[i]])
                i += 1

            queue.append(int(token))

        elif token in ('+', '-', '*', '/'):
            while stask and op_precedence(token) < op_precedence(stask[-1]):
                queue.append(stask.pop())

            stask.append(token)

        elif token == '(':
            stask.append(token)

        elif token == ')':
            while True:
                if not stask:
                    raise Exception('brackets mismatched')

                top = stask.pop()
                if top == '(':
                    break

                queue.append(top)

        elif token == ' ':
            pass

        else:
            raise Exception('undefined token', token)

    while stask:
        queue.append(stask.pop())

    return queue


def calc_rpn(rpn):
    """
    calculate expression in reverse polish notation

    >>> calc_rpn([2, 3, '+'])
    5

    >>> calc_rpn([2, 3, '-'])
    -1

    >>> calc_rpn([2, 3, '*'])
    6

    >>> calc_rpn([2, 3, 5, '*', '+'])
    17

    >>> calc_rpn([2, 3, '+', 5, '*'])
    25

    """
    stask = []

    ops = {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '*': lambda x, y: x * y,
        '/': lambda x, y: x / y
    }

    for token in rpn:
        if isinstance(token, int):
            stask.append(token)
        else:
            arg2, arg1 = stask.pop(), stask.pop()
            stask.append(ops[token](arg1, arg2))

    return stask.pop()


if __name__ == '__main__':
    doctest.testmod()

    while True:
        infix_notation = input('expression: ')

        try:
            postfix_notation = covert_to_rpn(infix_notation)
            print('reverse postfix notation: {}'.format(postfix_notation))

            result = calc_rpn(postfix_notation)
            print('{} = {}'.format(infix_notation, result))

        except Exception as e:
            print('wrong expression: {}\n {}'.format(infix_notation, e))
